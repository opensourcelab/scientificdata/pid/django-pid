ARG PYTHON_BASE

FROM python:${PYTHON_BASE}-slim-bookworm as base
ARG PYPI_URL

RUN mkdir /django_pid
WORKDIR /django_pid

# copy build files
COPY pyproject.toml poetry.lock README.md setup.cfg setup.py VERSION /django_pid/
COPY django_pid /django_pid/django_pid

ENV PYTHONDONTWRITEBYTECODE=1 \
    PYTHONUNBUFFERED=1 \
    POETRY_NO_INTERACTION=1 \
    POETRY_VIRTUALENVS_CREATE=0 \
    PIP_INDEX_URL=$PYPI_URL \
    PIP_CACHE_DIR=/var/cache/pip \
    POETRY_CACHE_DIR=/var/cache/poetry

#RUN sed -i 's|http://|https://artifactory.aws.gel.ac/artifactory/apt_|g' /etc/apt/sources.list

RUN \
    --mount=type=cache,target=/var/cache/apt \
    apt-get update -qq && apt-get install -qqy -f \
    git \
    pip

RUN \
    --mount=type=cache,target=${PIP_CACHE_DIR},sharing=private \
    mkdir -p ${PIP_CACHE_DIR} && \
    pip install -Iv --prefer-binary --index-url $PYPI_URL --upgrade \
    setuptools \
    poetry==1.8.3 \
    poetry-plugin-export

# Use poetry to resolve dependencies, output to requirements.txt and requirements_dev.txt, and pip to install
RUN  \
    --mount=type=cache,target=${PIP_CACHE_DIR},sharing=private \
    poetry export --only main --without-hashes -f requirements.txt -o requirements.txt \
    && poetry export --only dev --without-hashes -f requirements.txt -o requirements_dev.txt \
    && python -m pip install --prefer-binary --index-url $PYPI_URL -r requirements.txt \
    && python -m pip install --prefer-binary --index-url $PYPI_URL -e .

FROM base as test
ARG PYPI_URL

WORKDIR /django_pid
COPY tests /django_pid/tests

# required to make sure pytest runs the right coverage checks
ENV PYTHONPATH .

RUN \
    --mount=type=cache,target=${PIP_CACHE_DIR},sharing=private \
    python -m pip install --prefer-binary --index-url $PYPI_URL -r requirements_dev.txt .[tests]
