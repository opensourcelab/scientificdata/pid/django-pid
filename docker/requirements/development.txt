# it is recommended to load all production dependencies during development
-r production.txt


pytest==8.3.3
# pytest-asyncio==0.24.0
ruff>=0.5
uv>=0.4