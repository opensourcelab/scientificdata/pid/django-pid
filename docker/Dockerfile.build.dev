# django_pid development Dockerfile
# to build the docker image, call :
# docker build -f Dockerfile.dev -t my_project/django-pid-dev:0.0.1 .
# docker run -p 8000:8000 --name django_pid-dev my_project/django-pid-dev:0.0.1 
# see also https://snyk.io/blog/best-practices-containerizing-python-docker/

# to get the digest (=sha fingerprint) run:
# docker images --digests | grep python

# consider to create a django_build_image_base, store it in the registry and build from there, if caching is not working 

FROM python:3.12.5-slim-bookworm AS build_image
# to minimize image size one could build like (https://mherman.org/presentations/dockercon-2018/#39):
# using wheels

# set Docker Metadata

LABEL maintainer="mark doerr <mark.doerr@uni-greifswald.de>"
# 0.0.1
LABEL version=$VERSION 
LABEL description="django_pid - A *Python Django* *Persistent Identifier* (PID) app to abstract from PID services, like handle, DOI, etc. (build image)"


ENV LANG=C.UTF-8\
    PYTHONUNBUFFERED=1 \
    PYTHONDONTWRITEBYTECODE=1\
    PROJECT_ROOT=/opt/django_pid

ENV VIRTUAL_ENV=$PROJECT_ROOT/venv \
    DEBIAN_FRONTEND=noninteractive 

# Debian package caching
ARG APT_PROXY
RUN set -x \
    && if [ "${APT_PROXY}" ]; \
    then echo "Acquire::http { Proxy \"http://${APT_PROXY}\"; };" > /etc/apt/apt.conf.d/01proxy \
    ; fi

# build environment
RUN apt-get update \
    && apt-get install -y --no-install-recommends git build-essential gcc 

RUN python3 -m venv $VIRTUAL_ENV 

# Install dependencies.
COPY requirements "${PROJECT_ROOT}/requirements/"

# activating the virtual environment
ENV PATH="$VIRTUAL_ENV/bin:$PATH"

# install base dependencies
RUN pip install --no-cache-dir --upgrade pip \
    && pip install --no-cache-dir -r $PROJECT_ROOT/requirements/base.txt
    # && pip wheel --no-cache-dir --no-deps --wheel-dir /wheels -r $PROJECT_ROOT/requirements/base.txt

   

