"""_____________________________________________________________________

:PROJECT: LARAsuite

*django_pid app *

:details: django_pid app configuration. 
         This provides a generic django app configuration mechanism.
         For more details see:
         https://docs.djangoproject.com/en/4.0/ref/applications/
         - 
:authors: mark doerr <mark.doerr@uni-greifswald.de>

.. note:: -
.. todo:: - 
________________________________________________________________________
"""


from django.apps import AppConfig


class djangoPIDConfig(AppConfig):
    name = 'django_pid'
    # enter a verbose name for your app: django_pid here - this will be used in the admin interface
    verbose_name = 'django-pid'
    # lara_app_icon = 'django_pid_icon.svg'  # this will be used to display an icon, e.g. in the main LARA menu.
