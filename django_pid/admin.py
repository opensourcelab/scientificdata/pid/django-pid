"""_____________________________________________________________________

:PROJECT: LARAsuite

*django_pid admin *

:details: django_pid admin module admin backend configuration.
         - 
:authors: mark doerr <mark.doerr@uni-greifswald.de>

.. note:: -
.. todo:: - run "lara-django-dev admin_generator django_pid >> admin.py" to update this file
________________________________________________________________________
"""
