"""_____________________________________________________________________

:PROJECT: LARAsuite

*django_pid models *

:details: django_pid database models.
         - 
:authors: mark doerr <mark.doerr@uni-greifswald.de>
          David Linke 


.. note:: -
.. todo:: - 
________________________________________________________________________
"""

import logging
import datetime
import uuid

from django.utils import timezone
from django.conf import settings
from django.utils.translation import gettext_lazy as _

from django.db import models


settings.FIXTURES += []

logger = logging.getLogger(__name__)


class PIDServer(models.Model):
    """Definition of the PID servers, like handle, doi, ark, etc., at which PIDs are registered."""

    pid_server_id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=255, blank=True, null=True, unique=True, help_text="Name of the PID server")
    server_url = models.URLField(blank=True, null=True, unique=True, help_text="URL of the PID server")
    prefix = models.CharField(max_length=255, blank=True, null=True, unique=True, help_text="Prefix of the PID server")
    url_pattern = models.TextField(blank=True, null=True, unique=True, help_text="pattern of the PID URLs")
    description = models.TextField(blank=True, null=True, help_text="Description of the PID server")
    connection_parameters = models.JSONField(blank=True, null=True, help_text="Connection parameters for the PID server, like username, password/token, auth method, etc.")

class PIDType(models.Model):
    """Type of the PID, like handle, doi, ark, etc."""

    pid_type_id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=255, blank=True, null=True, unique=True, help_text="Name of the PID type")
    description = models.TextField(blank=True, null=True, help_text="Description of the PID type")

class PID(models.Model):
    """PID management queue. Here the PIDs are stored, which should be reserved and asynchronuously registered."""

    pid_id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    pid_type = models.ForeignKey(PIDType, on_delete=models.CASCADE, blank=True, null=True)
    pid = models.URLField(blank=True, null=True, unique=True, help_text="URL of the PID")
    visible = models.BooleanField(default=True, help_text="PID is resolvable")
    reserved = models.BooleanField(default=False, help_text="PID is reserved for usage, but not assigned to a resource, yet.")
    registered = models.BooleanField(default=False, help_text="PID is registered")

    metadata = models.JSONField(blank=True, null=True, help_text="Metadata of the PID specified as JSON")



