"""_____________________________________________________________________

:PROJECT: LARAsuite

*django_pid views *

:details: django_pid views module.
         - add app specific urls here
         - 
:authors: mark doerr <mark.doerr@uni-greifswald.de>

.. note:: -
.. todo:: -
________________________________________________________________________
"""

from dataclasses import dataclass, field
from typing import List

from django.shortcuts import get_object_or_404, render, redirect
from django.http import HttpResponseRedirect, HttpResponse
from django.urls import reverse

from django.views.generic import DetailView, ListView, CreateView, UpdateView, DeleteView

from django_tables2 import SingleTableView

# from .forms import
# from .tables import

# Create your  django_pid views here.


@dataclass
class ItemMenu:
    menu_items:  List[dict] = field(default_factory=lambda: [
        {'name': 'item1',
         'path': 'django_pid:view1-name'},
        {'name': 'item2',
         'path': 'django_pid:view2-name'},
    ])
