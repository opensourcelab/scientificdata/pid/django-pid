# django PID

A *Python Django* *Persistent Identifier* (PID) app to abstract from PID services, like handle, DOI, etc.

## Features

## Installation

    pip install django_pid --index-url https://gitlab.com/api/v4/projects/<gitlab-project-id>/packages/pypi/simple

## Usage

    django_pid --help 

## Development

    git clone gitlab.com/opensourcelab/django-pid

    # create a virtual environment and activate it then run

    pip install -e .[dev]

    # run unittests

    invoke test   # use the invoke environment to manage development
    

## Documentation

The Documentation can be found here: [https://opensourcelab.gitlab.io/django-pid](https://opensourcelab.gitlab.io/django-pid) or [django-pid.gitlab.io](django_pid.gitlab.io/)


## Credits

This package was created with [Cookiecutter](https://github.com/audreyr/cookiecutter)
 and the [gitlab.com/opensourcelab/software-dev/cookiecutter-pypackage](https://gitlab.com/opensourcelab/software-dev/cookiecutter-pypackage) project template.



