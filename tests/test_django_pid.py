#!/usr/bin/env python
"""Tests for `django_pid` package."""
# pylint: disable=redefined-outer-name
from django_pid.django_pid_interface import GreeterInterface
from django_pid.django_pid_impl import HelloWorld

def test_GreeterInterface():
    """ testing the formal interface (GreeterInterface)
    """
    assert issubclass(HelloWorld, GreeterInterface)

def test_HelloWorld():
    """ Testing HelloWorld class
    """
    hw = HelloWorld()
    name = 'yvain'
    assert hw.greet_the_world(name) == f"Hello world, {name} !"

