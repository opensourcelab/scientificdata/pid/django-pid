"""_____________________________________________________________________

:PROJECT: LARAsuite

*django_pid tests *

:details: django_pid application views tests.
         - 
:authors: mark doerr  <mark.doerr@uni-greifswald.de>

.. note:: -
.. todo:: - 
________________________________________________________________________
"""

from django.test import TestCase

import pytest
from django.conf import settings
from django.contrib import messages
from django.contrib.auth.models import AnonymousUser
from django.contrib.messages.middleware import MessageMiddleware
from django.contrib.sessions.middleware import SessionMiddleware
from django.http import HttpRequest, HttpResponseRedirect
from django.test import RequestFactory
from django.urls import reverse


# from django_pid.models import

# Create your django_pid tests here.

pytestmark = pytest.mark.django_db
