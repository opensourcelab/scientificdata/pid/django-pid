"""_____________________________________________________________________

:PROJECT: LARAsuite

*django_pid tests *

:details: django_pid application urls tests.
         - 
:authors: mark doerr  <mark.doerr@uni-greifswald.de>

.. note:: -
.. todo:: - 
________________________________________________________________________
"""

from django.test import TestCase
from django.urls import resolve, reverse

# from django_pid.models import

# Create your django_pid tests here.
